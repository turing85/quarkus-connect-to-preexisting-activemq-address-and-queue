package de.turing85;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/messages")
@Produces(MediaType.TEXT_PLAIN)
@Consumes(MediaType.TEXT_PLAIN)
public class TestResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestResource.class);

    private final Emitter<String> emitter;

    public TestResource(@Channel("message-out") Emitter<String> emitter) {
        this.emitter = emitter;
    }

    @POST
    public String postNewMessage(String message) {
        LOGGER.info("Received message: \"{}\" through rest-endpoint.", message);
        LOGGER.info("sending message \"{}\" to message-out channel", message);
        emitter.send(message);
        return message;
    }

    @Incoming("message-in")
    public void receiveMessage(String message) {
        LOGGER.info("Received message: \"{}\" through message-in channel.", message);
    }
}