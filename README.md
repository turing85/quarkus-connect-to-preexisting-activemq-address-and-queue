# quarkus-connect-to-preexisting-activemq-address-and-queue Project

## Scenario
We have an ArtemisMQ server, preconfigured with 

- an address `foo`, routing-type `MULTICAST`, and
- a queue `bar`, bound tot address `foo`, routing-tpe `MULTICAST`.

Our objective is to connect both our incoming and outgoing channels to the
address and queue respectively.

## Setup
in folder [`localdeployment`][localdeployment], we find a 
[`docker-compose.yml`][dockerCompose] which we can use to start-up our 
ActiveMQ-server for testing. The server has been preconfigured to provide 
aforementioned address and topic. If you are interested in the details, please
see file 
[`localdeployment/activemq/etc-override/broker-00.xml`][brokerOverride].

To start the ActiveMQ-server, we run

    cd localdeployment && docker-compose up -d

When the ActiveMQ-server is started, we can access its web-ui under 
[`http://localhost:8161/console`][activeMqConsole]. Login credentials are

- username: `quarkus`
- password: `quarkus`

We see, that address and queue are preconfigured as described:

![activeMqConfig][activeMqConfig]

## Configuration
We start by configuring the basic artemis configuration in: 
[`application.yml`][applicationYml]:

    amqp-host: ${AMQP_HOST:localhost}
    amqp-port: ${AMQP_PORT:5672}
    amqp-username: ${AMQP_USERNAME:quarkus}
    amqp-password: ${AMQP_PASSWORD:quarkus}

Next, we configure the outgoing channel in [`application.yml`][applicationYml]:

    mp:
      messaging:
        outgoing:
          message-out:
            address: ${AMQP_ADDRESS:foo}
            connector: smallrye-amqp
            durable: true

Important here is the property `address`. It must be set to the AMQP address.

Finally, we configure the incoming channel in 
[`application.yml`][applicationYml]:

    mp:
      messaging:
        ...
        incoming:
          message-in:
            address: ${AMQP_ADDRESS:foo}::${AMQP_QUEUE:bar}
            connector: smallrye-amqp
            durable: true

Important here, again, is the property `address`. For it, we have to specify the 
fully-qualified queue name (FQQN), i.e. `<ADDRESS_NAME>::<QUEUE_NAME>`. If we set the `address` to 
just the queue name, we will observe the following exception when the application starts:

    2021-07-06 19:27:14,839 WARN  [io.ver.amq.imp.AmqpReceiverImpl] (vert.x-eventloop-thread-0) Consumer for address bar unexpectedly closed remotely with error: io.vertx.core.impl.NoStackTraceThrowable: Error{condition=amqp:internal-error, description='Incorrect Routing Type for queue, expecting: ANYCAST', info=null}

## Test
Now that our application is properly configured, we can test our application. We
start the service:

    ./mvnw quarkus:dev

and send a request to the endpoint which will then be sent to ActiveMQ and 
received by our application. We can do this by accessing the swagger-ui at 
[`http://localhost:8080/q/swagger-ui`][swagger-ui]. We can send a message to our application through
the `POST` endpoint.

After sending a message, we should see the following or similar lines in the log of our application:

    2021-07-06 19:31:48,211 INFO  [de.tur.TestResource] (executor-thread-0) Received message: "A message." through rest-endpoint.
    2021-07-06 19:31:48,211 INFO  [de.tur.TestResource] (executor-thread-0) sending message "A message." to message-out channel
    2021-07-06 19:31:48,230 INFO  [de.tur.TestResource] (vert.x-eventloop-thread-0) Received message: "A message." through message-in channel.

[activeMqConfig]: images/configuration.png
[activeMqConsole]: http://localhost:8161/console
[applicationYml]: ./src/main/resources/application.yml
[brokerOverride]: localdeployment/activemq/etc-override/broker-00.xml
[dockerCompose]: localdeployment/docker-compose.yml
[localdeployment]: localdeployment
[swagger-ui]: http://localhost:8080/q/swagger-ui
